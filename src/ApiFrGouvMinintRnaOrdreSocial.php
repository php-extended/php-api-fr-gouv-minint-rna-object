<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

/**
 * ApiFrGouvMinintRnaOrdreSocial class file.
 * 
 * This is a simple implementation of the
 * ApiFrGouvMinintRnaOrdreSocialInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrGouvMinintRnaOrdreSocial implements ApiFrGouvMinintRnaOrdreSocialInterface
{
	
	/**
	 * The code of this ordre.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The name of this object.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Constructor for ApiFrGouvMinintRnaOrdreSocial with private members.
	 * 
	 * @param int $id
	 * @param string $name
	 */
	public function __construct(int $id, string $name)
	{
		$this->setId($id);
		$this->setName($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the code of this ordre.
	 * 
	 * @param int $id
	 * @return ApiFrGouvMinintRnaOrdreSocialInterface
	 */
	public function setId(int $id) : ApiFrGouvMinintRnaOrdreSocialInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the code of this ordre.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the name of this object.
	 * 
	 * @param string $name
	 * @return ApiFrGouvMinintRnaOrdreSocialInterface
	 */
	public function setName(string $name) : ApiFrGouvMinintRnaOrdreSocialInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of this object.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
