<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * MinintRnaNatureParser class file.
 * 
 * This class parses minint rna natures into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrGouvMinintRnaNature>
 */
class ApiFrGouvMinintRnaNatureParser extends AbstractParser
{
	
	/**
	 * The natures.
	 * 
	 * @var array<string, ApiFrGouvMinintRnaNature>
	 */
	protected array $_natures = [];
	
	/**
	 * Builds a new MinintRnaNatureParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/nature.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrGouvMinintRnaNature::class, $csvIterator);
		
		/** @var ApiFrGouvMinintRnaNature $nature */
		foreach($iterator as $nature)
		{
			$this->_natures[(string) $nature->getCode()] = $nature;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrGouvMinintRnaNature
	{
		$data = \trim((string) $data);
		
		if(empty($data))
		{
			$data = 'D';
		}
		
		if(isset($this->_natures[$data]))
		{
			return $this->_natures[$data];
		}
		
		throw new ParseException(ApiFrGouvMinintRnaNature::class, $data, 0);
	}
	
}
