<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

/**
 * ApiFrGouvMinintRnaGroupement class file.
 * 
 * This is a simple implementation of the
 * ApiFrGouvMinintRnaGroupementInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrGouvMinintRnaGroupement implements ApiFrGouvMinintRnaGroupementInterface
{
	
	/**
	 * The code of this groupement.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The sigle of this groupement.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The name of this groupement.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Constructor for ApiFrGouvMinintRnaGroupement with private members.
	 * 
	 * @param int $id
	 * @param string $code
	 * @param string $name
	 */
	public function __construct(int $id, string $code, string $name)
	{
		$this->setId($id);
		$this->setCode($code);
		$this->setName($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the code of this groupement.
	 * 
	 * @param int $id
	 * @return ApiFrGouvMinintRnaGroupementInterface
	 */
	public function setId(int $id) : ApiFrGouvMinintRnaGroupementInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the code of this groupement.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the sigle of this groupement.
	 * 
	 * @param string $code
	 * @return ApiFrGouvMinintRnaGroupementInterface
	 */
	public function setCode(string $code) : ApiFrGouvMinintRnaGroupementInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the sigle of this groupement.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the name of this groupement.
	 * 
	 * @param string $name
	 * @return ApiFrGouvMinintRnaGroupementInterface
	 */
	public function setName(string $name) : ApiFrGouvMinintRnaGroupementInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of this groupement.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
