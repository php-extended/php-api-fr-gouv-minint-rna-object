<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrGouvMinintRnaAssociationWaldec class file.
 * 
 * This is a simple implementation of the
 * ApiFrGouvMinintRnaAssociationWaldecInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.ShortVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiFrGouvMinintRnaAssociationWaldec implements ApiFrGouvMinintRnaAssociationWaldecInterface
{
	
	/**
	 * The waldec identifier.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The old identifier.
	 * 
	 * @var string
	 */
	protected string $_idEx;
	
	/**
	 * The siret of the etablissement.
	 * 
	 * @var ?string
	 */
	protected ?string $_siret = null;
	
	/**
	 * The number "reconnaissance d'utilité publique".
	 * 
	 * @var ?string
	 */
	protected ?string $_rupMi = null;
	
	/**
	 * The code gestion of the prefecture.
	 * 
	 * @var string
	 */
	protected string $_gestion;
	
	/**
	 * The date when this association was created.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateCreat;
	
	/**
	 * The date when this association was declared.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateDecla;
	
	/**
	 * The date when this association was published.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_datePubli;
	
	/**
	 * The date when this association was dissolved.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateDisso = null;
	
	/**
	 * The nature of the association.
	 * 
	 * @var ApiFrGouvMinintRnaNatureInterface
	 */
	protected ApiFrGouvMinintRnaNatureInterface $_nature;
	
	/**
	 * The groupement of the association.
	 * 
	 * @var ApiFrGouvMinintRnaGroupementInterface
	 */
	protected ApiFrGouvMinintRnaGroupementInterface $_groupement;
	
	/**
	 * The titre of the association.
	 * 
	 * @var string
	 */
	protected string $_titre;
	
	/**
	 * The titre court of the association.
	 * 
	 * @var string
	 */
	protected string $_titreCourt;
	
	/**
	 * The objet of the association.
	 * 
	 * @var string
	 */
	protected string $_objet;
	
	/**
	 * The 1st objet social of this association.
	 * 
	 * @var ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	protected ?ApiFrGouvMinintRnaObjetSocialInterface $_objetSocial1 = null;
	
	/**
	 * The 2nd objet social of this association.
	 * 
	 * @var ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	protected ?ApiFrGouvMinintRnaObjetSocialInterface $_objetSocial2 = null;
	
	/**
	 * Complement of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsComplement = null;
	
	/**
	 * Num voie of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsNumVoie = null;
	
	/**
	 * Repetition of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsRepetition = null;
	
	/**
	 * Type voie of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsTypeVoie = null;
	
	/**
	 * Libelle voie of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsLibvoie = null;
	
	/**
	 * Code Distrib of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsDistrib = null;
	
	/**
	 * Code insee of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsCodeinsee = null;
	
	/**
	 * Code postal of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsCodepostal = null;
	
	/**
	 * Libelle commune of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsLibcommune = null;
	
	/**
	 * Declarant of the adresse gestion.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrgDeclarant = null;
	
	/**
	 * Complement 1 of the adresse gestion.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrgComplemid = null;
	
	/**
	 * Complement 2 of the adresse gestion.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrgComplemgeo = null;
	
	/**
	 * Libelle voie of the adresse gestion.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrgLibvoie = null;
	
	/**
	 * Code Distrib of the adresse gestion.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrgDistrib = null;
	
	/**
	 * Code postal of the adresse gestion.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrgCodepostal = null;
	
	/**
	 * Acheminement of the adresse gestion.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrgAchemine = null;
	
	/**
	 * Country of the adresse gestion.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrgPays = null;
	
	/**
	 * Civilite of the dirigeant.
	 * 
	 * @var string
	 */
	protected string $_dirCivilite;
	
	/**
	 * The uri of the website = null; if any.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_siteweb = null;
	
	/**
	 * True if the association allows publishing on the web.
	 * 
	 * @var bool
	 */
	protected bool $_publiweb;
	
	/**
	 * The observations on this association.
	 * 
	 * @var ?string
	 */
	protected ?string $_observation = null;
	
	/**
	 * The position of this association.
	 * 
	 * @var ApiFrGouvMinintRnaPositionInterface
	 */
	protected ApiFrGouvMinintRnaPositionInterface $_position;
	
	/**
	 * The date of last update.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_majTime;
	
	/**
	 * Constructor for ApiFrGouvMinintRnaAssociationWaldec with private members.
	 * 
	 * @param string $id
	 * @param string $idEx
	 * @param string $gestion
	 * @param DateTimeInterface $dateCreat
	 * @param DateTimeInterface $dateDecla
	 * @param DateTimeInterface $datePubli
	 * @param ApiFrGouvMinintRnaNatureInterface $nature
	 * @param ApiFrGouvMinintRnaGroupementInterface $groupement
	 * @param string $titre
	 * @param string $titreCourt
	 * @param string $objet
	 * @param string $dirCivilite
	 * @param bool $publiweb
	 * @param ApiFrGouvMinintRnaPositionInterface $position
	 * @param DateTimeInterface $majTime
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(string $id, string $idEx, string $gestion, DateTimeInterface $dateCreat, DateTimeInterface $dateDecla, DateTimeInterface $datePubli, ApiFrGouvMinintRnaNatureInterface $nature, ApiFrGouvMinintRnaGroupementInterface $groupement, string $titre, string $titreCourt, string $objet, string $dirCivilite, bool $publiweb, ApiFrGouvMinintRnaPositionInterface $position, DateTimeInterface $majTime)
	{
		$this->setId($id);
		$this->setIdEx($idEx);
		$this->setGestion($gestion);
		$this->setDateCreat($dateCreat);
		$this->setDateDecla($dateDecla);
		$this->setDatePubli($datePubli);
		$this->setNature($nature);
		$this->setGroupement($groupement);
		$this->setTitre($titre);
		$this->setTitreCourt($titreCourt);
		$this->setObjet($objet);
		$this->setDirCivilite($dirCivilite);
		$this->setPubliweb($publiweb);
		$this->setPosition($position);
		$this->setMajTime($majTime);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the waldec identifier.
	 * 
	 * @param string $id
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setId(string $id) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the waldec identifier.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the old identifier.
	 * 
	 * @param string $idEx
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setIdEx(string $idEx) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_idEx = $idEx;
		
		return $this;
	}
	
	/**
	 * Gets the old identifier.
	 * 
	 * @return string
	 */
	public function getIdEx() : string
	{
		return $this->_idEx;
	}
	
	/**
	 * Sets the siret of the etablissement.
	 * 
	 * @param ?string $siret
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setSiret(?string $siret) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_siret = $siret;
		
		return $this;
	}
	
	/**
	 * Gets the siret of the etablissement.
	 * 
	 * @return ?string
	 */
	public function getSiret() : ?string
	{
		return $this->_siret;
	}
	
	/**
	 * Sets the number "reconnaissance d'utilité publique".
	 * 
	 * @param ?string $rupMi
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setRupMi(?string $rupMi) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_rupMi = $rupMi;
		
		return $this;
	}
	
	/**
	 * Gets the number "reconnaissance d'utilité publique".
	 * 
	 * @return ?string
	 */
	public function getRupMi() : ?string
	{
		return $this->_rupMi;
	}
	
	/**
	 * Sets the code gestion of the prefecture.
	 * 
	 * @param string $gestion
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setGestion(string $gestion) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_gestion = $gestion;
		
		return $this;
	}
	
	/**
	 * Gets the code gestion of the prefecture.
	 * 
	 * @return string
	 */
	public function getGestion() : string
	{
		return $this->_gestion;
	}
	
	/**
	 * Sets the date when this association was created.
	 * 
	 * @param DateTimeInterface $dateCreat
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setDateCreat(DateTimeInterface $dateCreat) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_dateCreat = $dateCreat;
		
		return $this;
	}
	
	/**
	 * Gets the date when this association was created.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateCreat() : DateTimeInterface
	{
		return $this->_dateCreat;
	}
	
	/**
	 * Sets the date when this association was declared.
	 * 
	 * @param DateTimeInterface $dateDecla
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setDateDecla(DateTimeInterface $dateDecla) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_dateDecla = $dateDecla;
		
		return $this;
	}
	
	/**
	 * Gets the date when this association was declared.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDecla() : DateTimeInterface
	{
		return $this->_dateDecla;
	}
	
	/**
	 * Sets the date when this association was published.
	 * 
	 * @param DateTimeInterface $datePubli
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setDatePubli(DateTimeInterface $datePubli) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_datePubli = $datePubli;
		
		return $this;
	}
	
	/**
	 * Gets the date when this association was published.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDatePubli() : DateTimeInterface
	{
		return $this->_datePubli;
	}
	
	/**
	 * Sets the date when this association was dissolved.
	 * 
	 * @param ?DateTimeInterface $dateDisso
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setDateDisso(?DateTimeInterface $dateDisso) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_dateDisso = $dateDisso;
		
		return $this;
	}
	
	/**
	 * Gets the date when this association was dissolved.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDisso() : ?DateTimeInterface
	{
		return $this->_dateDisso;
	}
	
	/**
	 * Sets the nature of the association.
	 * 
	 * @param ApiFrGouvMinintRnaNatureInterface $nature
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setNature(ApiFrGouvMinintRnaNatureInterface $nature) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_nature = $nature;
		
		return $this;
	}
	
	/**
	 * Gets the nature of the association.
	 * 
	 * @return ApiFrGouvMinintRnaNatureInterface
	 */
	public function getNature() : ApiFrGouvMinintRnaNatureInterface
	{
		return $this->_nature;
	}
	
	/**
	 * Sets the groupement of the association.
	 * 
	 * @param ApiFrGouvMinintRnaGroupementInterface $groupement
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setGroupement(ApiFrGouvMinintRnaGroupementInterface $groupement) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_groupement = $groupement;
		
		return $this;
	}
	
	/**
	 * Gets the groupement of the association.
	 * 
	 * @return ApiFrGouvMinintRnaGroupementInterface
	 */
	public function getGroupement() : ApiFrGouvMinintRnaGroupementInterface
	{
		return $this->_groupement;
	}
	
	/**
	 * Sets the titre of the association.
	 * 
	 * @param string $titre
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setTitre(string $titre) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_titre = $titre;
		
		return $this;
	}
	
	/**
	 * Gets the titre of the association.
	 * 
	 * @return string
	 */
	public function getTitre() : string
	{
		return $this->_titre;
	}
	
	/**
	 * Sets the titre court of the association.
	 * 
	 * @param string $titreCourt
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setTitreCourt(string $titreCourt) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_titreCourt = $titreCourt;
		
		return $this;
	}
	
	/**
	 * Gets the titre court of the association.
	 * 
	 * @return string
	 */
	public function getTitreCourt() : string
	{
		return $this->_titreCourt;
	}
	
	/**
	 * Sets the objet of the association.
	 * 
	 * @param string $objet
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setObjet(string $objet) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_objet = $objet;
		
		return $this;
	}
	
	/**
	 * Gets the objet of the association.
	 * 
	 * @return string
	 */
	public function getObjet() : string
	{
		return $this->_objet;
	}
	
	/**
	 * Sets the 1st objet social of this association.
	 * 
	 * @param ?ApiFrGouvMinintRnaObjetSocialInterface $objetSocial1
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setObjetSocial1(?ApiFrGouvMinintRnaObjetSocialInterface $objetSocial1) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_objetSocial1 = $objetSocial1;
		
		return $this;
	}
	
	/**
	 * Gets the 1st objet social of this association.
	 * 
	 * @return ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function getObjetSocial1() : ?ApiFrGouvMinintRnaObjetSocialInterface
	{
		return $this->_objetSocial1;
	}
	
	/**
	 * Sets the 2nd objet social of this association.
	 * 
	 * @param ?ApiFrGouvMinintRnaObjetSocialInterface $objetSocial2
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setObjetSocial2(?ApiFrGouvMinintRnaObjetSocialInterface $objetSocial2) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_objetSocial2 = $objetSocial2;
		
		return $this;
	}
	
	/**
	 * Gets the 2nd objet social of this association.
	 * 
	 * @return ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function getObjetSocial2() : ?ApiFrGouvMinintRnaObjetSocialInterface
	{
		return $this->_objetSocial2;
	}
	
	/**
	 * Sets complement of the adresse siege.
	 * 
	 * @param ?string $adrsComplement
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsComplement(?string $adrsComplement) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsComplement = $adrsComplement;
		
		return $this;
	}
	
	/**
	 * Gets complement of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsComplement() : ?string
	{
		return $this->_adrsComplement;
	}
	
	/**
	 * Sets num voie of the adresse siege.
	 * 
	 * @param ?string $adrsNumVoie
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsNumVoie(?string $adrsNumVoie) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsNumVoie = $adrsNumVoie;
		
		return $this;
	}
	
	/**
	 * Gets num voie of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsNumVoie() : ?string
	{
		return $this->_adrsNumVoie;
	}
	
	/**
	 * Sets repetition of the adresse siege.
	 * 
	 * @param ?string $adrsRepetition
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsRepetition(?string $adrsRepetition) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsRepetition = $adrsRepetition;
		
		return $this;
	}
	
	/**
	 * Gets repetition of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsRepetition() : ?string
	{
		return $this->_adrsRepetition;
	}
	
	/**
	 * Sets type voie of the adresse siege.
	 * 
	 * @param ?string $adrsTypeVoie
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsTypeVoie(?string $adrsTypeVoie) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsTypeVoie = $adrsTypeVoie;
		
		return $this;
	}
	
	/**
	 * Gets type voie of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsTypeVoie() : ?string
	{
		return $this->_adrsTypeVoie;
	}
	
	/**
	 * Sets libelle voie of the adresse siege.
	 * 
	 * @param ?string $adrsLibvoie
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsLibvoie(?string $adrsLibvoie) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsLibvoie = $adrsLibvoie;
		
		return $this;
	}
	
	/**
	 * Gets libelle voie of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsLibvoie() : ?string
	{
		return $this->_adrsLibvoie;
	}
	
	/**
	 * Sets code Distrib of the adresse siege.
	 * 
	 * @param ?string $adrsDistrib
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsDistrib(?string $adrsDistrib) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsDistrib = $adrsDistrib;
		
		return $this;
	}
	
	/**
	 * Gets code Distrib of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsDistrib() : ?string
	{
		return $this->_adrsDistrib;
	}
	
	/**
	 * Sets code insee of the adresse siege.
	 * 
	 * @param ?string $adrsCodeinsee
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsCodeinsee(?string $adrsCodeinsee) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsCodeinsee = $adrsCodeinsee;
		
		return $this;
	}
	
	/**
	 * Gets code insee of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsCodeinsee() : ?string
	{
		return $this->_adrsCodeinsee;
	}
	
	/**
	 * Sets code postal of the adresse siege.
	 * 
	 * @param ?string $adrsCodepostal
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsCodepostal(?string $adrsCodepostal) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsCodepostal = $adrsCodepostal;
		
		return $this;
	}
	
	/**
	 * Gets code postal of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsCodepostal() : ?string
	{
		return $this->_adrsCodepostal;
	}
	
	/**
	 * Sets libelle commune of the adresse siege.
	 * 
	 * @param ?string $adrsLibcommune
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrsLibcommune(?string $adrsLibcommune) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrsLibcommune = $adrsLibcommune;
		
		return $this;
	}
	
	/**
	 * Gets libelle commune of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsLibcommune() : ?string
	{
		return $this->_adrsLibcommune;
	}
	
	/**
	 * Sets declarant of the adresse gestion.
	 * 
	 * @param ?string $adrgDeclarant
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrgDeclarant(?string $adrgDeclarant) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrgDeclarant = $adrgDeclarant;
		
		return $this;
	}
	
	/**
	 * Gets declarant of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgDeclarant() : ?string
	{
		return $this->_adrgDeclarant;
	}
	
	/**
	 * Sets complement 1 of the adresse gestion.
	 * 
	 * @param ?string $adrgComplemid
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrgComplemid(?string $adrgComplemid) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrgComplemid = $adrgComplemid;
		
		return $this;
	}
	
	/**
	 * Gets complement 1 of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgComplemid() : ?string
	{
		return $this->_adrgComplemid;
	}
	
	/**
	 * Sets complement 2 of the adresse gestion.
	 * 
	 * @param ?string $adrgComplemgeo
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrgComplemgeo(?string $adrgComplemgeo) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrgComplemgeo = $adrgComplemgeo;
		
		return $this;
	}
	
	/**
	 * Gets complement 2 of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgComplemgeo() : ?string
	{
		return $this->_adrgComplemgeo;
	}
	
	/**
	 * Sets libelle voie of the adresse gestion.
	 * 
	 * @param ?string $adrgLibvoie
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrgLibvoie(?string $adrgLibvoie) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrgLibvoie = $adrgLibvoie;
		
		return $this;
	}
	
	/**
	 * Gets libelle voie of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgLibvoie() : ?string
	{
		return $this->_adrgLibvoie;
	}
	
	/**
	 * Sets code Distrib of the adresse gestion.
	 * 
	 * @param ?string $adrgDistrib
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrgDistrib(?string $adrgDistrib) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrgDistrib = $adrgDistrib;
		
		return $this;
	}
	
	/**
	 * Gets code Distrib of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgDistrib() : ?string
	{
		return $this->_adrgDistrib;
	}
	
	/**
	 * Sets code postal of the adresse gestion.
	 * 
	 * @param ?string $adrgCodepostal
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrgCodepostal(?string $adrgCodepostal) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrgCodepostal = $adrgCodepostal;
		
		return $this;
	}
	
	/**
	 * Gets code postal of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgCodepostal() : ?string
	{
		return $this->_adrgCodepostal;
	}
	
	/**
	 * Sets acheminement of the adresse gestion.
	 * 
	 * @param ?string $adrgAchemine
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrgAchemine(?string $adrgAchemine) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrgAchemine = $adrgAchemine;
		
		return $this;
	}
	
	/**
	 * Gets acheminement of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgAchemine() : ?string
	{
		return $this->_adrgAchemine;
	}
	
	/**
	 * Sets country of the adresse gestion.
	 * 
	 * @param ?string $adrgPays
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setAdrgPays(?string $adrgPays) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_adrgPays = $adrgPays;
		
		return $this;
	}
	
	/**
	 * Gets country of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgPays() : ?string
	{
		return $this->_adrgPays;
	}
	
	/**
	 * Sets civilite of the dirigeant.
	 * 
	 * @param string $dirCivilite
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setDirCivilite(string $dirCivilite) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_dirCivilite = $dirCivilite;
		
		return $this;
	}
	
	/**
	 * Gets civilite of the dirigeant.
	 * 
	 * @return string
	 */
	public function getDirCivilite() : string
	{
		return $this->_dirCivilite;
	}
	
	/**
	 * Sets the uri of the website = null; if any.
	 * 
	 * @param ?UriInterface $siteweb
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setSiteweb(?UriInterface $siteweb) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_siteweb = $siteweb;
		
		return $this;
	}
	
	/**
	 * Gets the uri of the website = null; if any.
	 * 
	 * @return ?UriInterface
	 */
	public function getSiteweb() : ?UriInterface
	{
		return $this->_siteweb;
	}
	
	/**
	 * Sets true if the association allows publishing on the web.
	 * 
	 * @param bool $publiweb
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setPubliweb(bool $publiweb) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_publiweb = $publiweb;
		
		return $this;
	}
	
	/**
	 * Gets true if the association allows publishing on the web.
	 * 
	 * @return bool
	 */
	public function hasPubliweb() : bool
	{
		return $this->_publiweb;
	}
	
	/**
	 * Sets the observations on this association.
	 * 
	 * @param ?string $observation
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setObservation(?string $observation) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_observation = $observation;
		
		return $this;
	}
	
	/**
	 * Gets the observations on this association.
	 * 
	 * @return ?string
	 */
	public function getObservation() : ?string
	{
		return $this->_observation;
	}
	
	/**
	 * Sets the position of this association.
	 * 
	 * @param ApiFrGouvMinintRnaPositionInterface $position
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setPosition(ApiFrGouvMinintRnaPositionInterface $position) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_position = $position;
		
		return $this;
	}
	
	/**
	 * Gets the position of this association.
	 * 
	 * @return ApiFrGouvMinintRnaPositionInterface
	 */
	public function getPosition() : ApiFrGouvMinintRnaPositionInterface
	{
		return $this->_position;
	}
	
	/**
	 * Sets the date of last update.
	 * 
	 * @param DateTimeInterface $majTime
	 * @return ApiFrGouvMinintRnaAssociationWaldecInterface
	 */
	public function setMajTime(DateTimeInterface $majTime) : ApiFrGouvMinintRnaAssociationWaldecInterface
	{
		$this->_majTime = $majTime;
		
		return $this;
	}
	
	/**
	 * Gets the date of last update.
	 * 
	 * @return DateTimeInterface
	 */
	public function getMajTime() : DateTimeInterface
	{
		return $this->_majTime;
	}
	
}
