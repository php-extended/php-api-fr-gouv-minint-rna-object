<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

/**
 * ApiFrGouvMinintRnaObjetSocial class file.
 * 
 * This is a simple implementation of the
 * ApiFrGouvMinintRnaObjetSocialInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrGouvMinintRnaObjetSocial implements ApiFrGouvMinintRnaObjetSocialInterface
{
	
	/**
	 * The code of this object.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The ordre social of this object.
	 * 
	 * @var ApiFrGouvMinintRnaOrdreSocialInterface
	 */
	protected ApiFrGouvMinintRnaOrdreSocialInterface $_ordreSocial;
	
	/**
	 * The name of this object.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Constructor for ApiFrGouvMinintRnaObjetSocial with private members.
	 * 
	 * @param int $id
	 * @param ApiFrGouvMinintRnaOrdreSocialInterface $ordreSocial
	 * @param string $name
	 */
	public function __construct(int $id, ApiFrGouvMinintRnaOrdreSocialInterface $ordreSocial, string $name)
	{
		$this->setId($id);
		$this->setOrdreSocial($ordreSocial);
		$this->setName($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the code of this object.
	 * 
	 * @param int $id
	 * @return ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function setId(int $id) : ApiFrGouvMinintRnaObjetSocialInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the code of this object.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the ordre social of this object.
	 * 
	 * @param ApiFrGouvMinintRnaOrdreSocialInterface $ordreSocial
	 * @return ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function setOrdreSocial(ApiFrGouvMinintRnaOrdreSocialInterface $ordreSocial) : ApiFrGouvMinintRnaObjetSocialInterface
	{
		$this->_ordreSocial = $ordreSocial;
		
		return $this;
	}
	
	/**
	 * Gets the ordre social of this object.
	 * 
	 * @return ApiFrGouvMinintRnaOrdreSocialInterface
	 */
	public function getOrdreSocial() : ApiFrGouvMinintRnaOrdreSocialInterface
	{
		return $this->_ordreSocial;
	}
	
	/**
	 * Sets the name of this object.
	 * 
	 * @param string $name
	 * @return ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function setName(string $name) : ApiFrGouvMinintRnaObjetSocialInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of this object.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
