<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use Iterator;
use PhpExtended\DataProvider\CsvAcceptIterator;
use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\CsvStringDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use PhpExtended\Reifier\ReifierReportInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use Throwable;

/**
 * MinintRnaEndpoint class file.
 * 
 * This is a simple implementation of the ApiFrGouvMinintRnaEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class ApiFrGouvMinintRnaEndpoint implements ApiFrGouvMinintRnaEndpointInterface
{
	
	public const HOST = 'https://files.data.gouv.fr/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The temp directory path.
	 *
	 * @var string
	 */
	protected string $_tempDirectoryPath;
	
	/**
	 * Builds a new Endpoint with its dependancies.
	 *
	 * @param string $tempDirectoryPath
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct(
		string $tempDirectoryPath,
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$realpath = \realpath($tempDirectoryPath);
		if(false === $realpath)
		{
			$message = 'The given temp directory path "{dir}" does not exist on the filesystem';
			$context = ['{dir}' => $tempDirectoryPath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_dir($realpath))
		{
			$message = 'The given directory "{dir}" is not a real directory';
			$context = ['{dir}' => $realpath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_writable($realpath))
		{
			$message = 'The given directory "{dir}" is not writeable';
			$context = ['{dir}' => $realpath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		$this->_tempDirectoryPath = $realpath;
		
		$configuration = $this->_reifier->getConfiguration();
		$configuration->setParser(ApiFrGouvMinintRnaGroupement::class, new ApiFrGouvMinintRnaGroupementParser());
		$configuration->setParser(ApiFrGouvMinintRnaNature::class, new ApiFrGouvMinintRnaNatureParser());
		$configuration->setParser(ApiFrGouvMinintRnaOrdreSocial::class, $osp = new ApiFrGouvMinintRnaOrdreSocialParser());
		$configuration->setParser(ApiFrGouvMinintRnaObjetSocial::class, new ApiFrGouvMinintRnaObjetSocialParser($osp));
		$configuration->setParser(ApiFrGouvMinintRnaPosition::class, new ApiFrGouvMinintRnaPositionParser());
		
		$configuration->addDateTimeFormat(ApiFrGouvMinintRnaAssociationImport::class, 'dateCreat', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrGouvMinintRnaAssociationImport::class, 'datePubli', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrGouvMinintRnaAssociationImport::class, 'majTime', ['Y-m-d H:i:s', 'Y-m-d  H:i:s']);
		$configuration->addDateTimeFormat(ApiFrGouvMinintRnaAssociationWaldec::class, 'dateCreat', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrGouvMinintRnaAssociationWaldec::class, 'dateDecla', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrGouvMinintRnaAssociationWaldec::class, 'datePubli', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrGouvMinintRnaAssociationWaldec::class, 'dateDisso', ['Y-m-d']);
		$configuration->addDateTimeFormat(ApiFrGouvMinintRnaAssociationWaldec::class, 'majTime', ['Y-m-d H:i:s', 'Y-m-d  H:i:s']);
		
		$configuration->addFieldsAllowedToFail(ApiFrGouvMinintRnaAssociationImport::class, ['siteweb', 'objet_social1', 'objet_social2']);
		$configuration->addFieldsAllowedToFail(ApiFrGouvMinintRnaAssociationWaldec::class, ['siteweb', 'objet_social1', 'objet_social2']);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpointInterface::getLatestUploadDate()
	 */
	public function getLatestUploadDate() : DateTimeInterface
	{
		$url = 'https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations/';
		
		$json = $this->getPageJson($url);
		
		if(!isset($json['dateModified']) || !\is_string($json['dateModified']))
		{
			$message = 'Failed to find dateModified field in json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$date = DateTimeImmutable::createFromFormat('Y-m-d\\TH:i:s.u', $json['dateModified']);
		if(false === $date)
		{
			$message = 'Failed to parse date modified field in json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $date;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpointInterface::getLatestRnaImportIterator()
	 */
	public function getLatestRnaImportIterator(?ReifierReportInterface $report = null) : Iterator
	{
		$url = 'https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations/';
		
		$json = $this->getPageJson($url);
		
		if(!isset($json['distribution']) || !\is_array($json['distribution']))
		{
			$message = 'Failed to find distribution field in json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$documentUrl = $this->getDistributionUrl($json['distribution'], 'rna_import');
		if(null === $documentUrl)
		{
			$message = 'Failed to find a suitable last import url injson ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		try
		{
			$uri = $this->_uriFactory->createUri($documentUrl);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'rna_import.zip');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			$acceptLine = function(array $data) : bool
			{
				// files are given by departement, but the unzipping stacks them all
				// we have to remove the intermediary headers
				return isset($data['id_ex']) && 'id_ex' !== $data['id_ex'];
			};
			
			$csvIterator = empty($xDownloadedFile)
				? new CsvStringDataIterator($response->getBody()->__toString(), true, ';', '"', '\\', 'UTF-8', 'UTF-8')
				: new CsvFileDataIterator($xDownloadedFile, true, ';', '"', '\\', 'UTF-8', 'UTF-8');
			$acceptIterator = new CsvAcceptIterator($csvIterator, $acceptLine);

			return null === $report
				? $this->_reifier->reifyIterator(ApiFrGouvMinintRnaAssociationImport::class, $acceptIterator)
				: $this->_reifier->tryReifyIterator(ApiFrGouvMinintRnaAssociationImport::class, $acceptIterator, $report);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpointInterface::getLatestRnaWaldecIterator()
	 */
	public function getLatestRnaWaldecIterator(?ReifierReportInterface $report = null) : Iterator
	{
		$url = 'https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations/';
		
		$json = $this->getPageJson($url);
		
		if(!isset($json['distribution']) || !\is_array($json['distribution']))
		{
			$message = 'Failed to find distribution field in json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$documentUrl = $this->getDistributionUrl($json['distribution'], 'rna_waldec');
		if(null === $documentUrl)
		{
			$message = 'Failed to find a suitable last import url injson ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		try
		{
			$uri = $this->_uriFactory->createUri($documentUrl);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'rna_waldec.zip');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			$acceptLine = function(array $data) : bool
			{
				// files are given by departement, but the unzipping stacks them all
				// we have to remove the intermediary headers
				return isset($data['id_ex']) && 'id_ex' !== $data['id_ex'];
			};
			
			$csvIterator = empty($xDownloadedFile)
				? new CsvStringDataIterator($response->getBody()->__toString(), true, ';', '"', '\\', 'UTF-8', 'UTF-8')
				: new CsvFileDataIterator($xDownloadedFile, true, ';', '"', '\\', 'UTF-8', 'UTF-8');
			$acceptIterator = new CsvAcceptIterator($csvIterator, $acceptLine);
			
			return null === $report
				? $this->_reifier->reifyIterator(ApiFrGouvMinintRnaAssociationWaldec::class, $acceptIterator)
				: $this->_reifier->tryReifyIterator(ApiFrGouvMinintRnaAssociationWaldec::class, $acceptIterator, $report);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpointInterface::getNatureIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getNatureIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/nature.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrGouvMinintRnaNature::class, $csvIterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpointInterface::getGroupementIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getGroupementIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/groupement.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrGouvMinintRnaGroupement::class, $csvIterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpointInterface::getObjetSocialIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getObjetSocialIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/objet_social.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrGouvMinintRnaObjetSocial::class, $csvIterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpointInterface::getPositionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPositionIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/position.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrGouvMinintRnaPosition::class, $csvIterator);
	}
	
	/**
	 * Gets the distribution url in the given json data, searching for file names.
	 * 
	 * @param array<integer|string, integer|string|array<integer|string, integer|string>> $jsonDistribution
	 * @param string $searchFor
	 * @return ?string
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getDistributionUrl(array $jsonDistribution, string $searchFor) : ?string
	{
		/** @var DateTimeImmutable $lastDate */
		$lastDate = DateTimeImmutable::createFromFormat('Y-m-d', '2000-01-01');
		$documentUrl = null;
		
		foreach($jsonDistribution as $distribution)
		{
			if(!\is_array($distribution))
			{
				continue;
			}
			
			if(!isset($distribution['contentUrl']) || !\is_string($distribution['contentUrl']))
			{
				continue;
			}
			
			if(\mb_strpos($distribution['contentUrl'], $searchFor) === false)
			{
				continue;
			}
			
			if(!isset($distribution['dateModified']) || !\is_string($distribution['dateModified']))
			{
				continue;
			}
			
			$date = DateTimeImmutable::createFromFormat('Y-m-d\\TH:i:s.u', $distribution['dateModified']);
			if(false === $date)
			{
				$date = DateTimeImmutable::createFromFormat('Y-m-d\\TH:i:s', $distribution['dateModified']);
				if(false === $date)
				{
					continue;
				}
			}
			
			if($date->getTimestamp() > $lastDate->getTimestamp())
			{
				$lastDate = $date;
				$documentUrl = $distribution['contentUrl'];
			}
		}
		
		return $documentUrl;
	}
	
	/**
	 * Gets the json data from the data.gouv page.
	 * 
	 * @param string $url
	 * @return array<integer|string, integer|string|array<integer|string, integer|string|array<integer|string, integer|string>>>
	 * @throws RuntimeException
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	protected function getPageJson(string $url) : array
	{
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$data = $response->getBody()->__toString();
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		$needle = '<script id="json_ld" type="application/ld+json">';
		$pos = \mb_strpos($data, $needle);
		if(false === $pos)
		{
			$message = 'Failed to find json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$needle2 = '</script>';
		$pos2 = \mb_strpos($data, $needle2, $pos + 1);
		if(false === $pos2)
		{
			$message = 'Failed to find end of json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$jsonStr = \mb_substr($data, $pos + (int) \mb_strlen($needle), $pos2 - $pos - (int) \mb_strlen($needle));
		if(empty($jsonStr))
		{
			$message = 'Failed to extract json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$json = \json_decode($jsonStr, true);
		if(empty($json) || !\is_array($json))
		{
			$message = 'Failed to decode json ld script at url "{url}"';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		/** @psalm-suppress MixedReturnTypeCoercion */
		return $json;
	}
	
}
