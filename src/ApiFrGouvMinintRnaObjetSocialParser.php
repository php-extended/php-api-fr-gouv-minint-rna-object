<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use InvalidArgumentException;
use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * MinintRnaObjetSocialParser class file.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrGouvMinintRnaObjetSocial>
 */
class ApiFrGouvMinintRnaObjetSocialParser extends AbstractParser
{
	
	/**
	 * The objets sociaux.
	 * 
	 * @var array<integer, ApiFrGouvMinintRnaObjetSocial>
	 */
	protected array $_objetsSociaux = [];
	
	/**
	 * Builds a new MinintRnaObjetSocialParser with the given data.
	 * 
	 * @param ApiFrGouvMinintRnaOrdreSocialParser $parser
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct(ApiFrGouvMinintRnaOrdreSocialParser $parser)
	{
		$reifier = new Reifier();
		$reifier->getConfiguration()->setParser(ApiFrGouvMinintRnaOrdreSocial::class, $parser);
		$filePath = \dirname(__DIR__).'/data/objet_social.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrGouvMinintRnaObjetSocial::class, $csvIterator);
		
		/** @var ApiFrGouvMinintRnaObjetSocial $objetSocial */
		foreach($iterator as $objetSocial)
		{
			$this->_objetsSociaux[(int) $objetSocial->getId()] = $objetSocial;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrGouvMinintRnaObjetSocial
	{
		$pos = (int) \trim((string) $data);
		
		if(isset($this->_objetsSociaux[$pos]))
		{
			return $this->_objetsSociaux[$pos];
		}
		
		// @codeCoverageIgnoreStart
		throw new ParseException(ApiFrGouvMinintRnaObjetSocial::class, $data, 0);
		// @codeCoverageIgnoreEnd
	}
	
}
