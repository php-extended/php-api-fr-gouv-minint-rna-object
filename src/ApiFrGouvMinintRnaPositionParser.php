<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * MinintRnaPositionParser class file.
 * 
 * This class parses minint rna positions into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrGouvMinintRnaPosition>
 */
class ApiFrGouvMinintRnaPositionParser extends AbstractParser
{
	
	/**
	 * The positions.
	 * 
	 * @var array<string, ApiFrGouvMinintRnaPosition>
	 */
	protected array $_positions = [];
	
	/**
	 * Builds a new MinintRnaPositionParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/position.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrGouvMinintRnaPosition::class, $csvIterator);
		
		/** @var ApiFrGouvMinintRnaPosition $position */
		foreach($iterator as $position)
		{
			$this->_positions[(string) $position->getCode()] = $position;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrGouvMinintRnaPosition
	{
		$data = \trim((string) $data);
		
		if(empty($data))
		{
			$data = 'A';
		}
		
		if(isset($this->_positions[$data]))
		{
			return $this->_positions[$data];
		}
		
		throw new ParseException(ApiFrGouvMinintRnaPosition::class, $data, 0);
	}
	
}
