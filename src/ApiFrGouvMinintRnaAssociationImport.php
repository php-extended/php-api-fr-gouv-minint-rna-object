<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrGouvMinintRnaAssociationImport class file.
 * 
 * This is a simple implementation of the
 * ApiFrGouvMinintRnaAssociationImportInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.ShortVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiFrGouvMinintRnaAssociationImport implements ApiFrGouvMinintRnaAssociationImportInterface
{
	
	/**
	 * The old identifier.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The old² identifier.
	 * 
	 * @var string
	 */
	protected string $_idEx;
	
	/**
	 * The siret of the etablissement.
	 * 
	 * @var ?string
	 */
	protected ?string $_siret = null;
	
	/**
	 * The code gestion of the prefecture.
	 * 
	 * @var string
	 */
	protected string $_gestion;
	
	/**
	 * The date when this association was created.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateCreat;
	
	/**
	 * The date when this association was published.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_datePubli;
	
	/**
	 * The nature of the association.
	 * 
	 * @var ApiFrGouvMinintRnaNatureInterface
	 */
	protected ApiFrGouvMinintRnaNatureInterface $_nature;
	
	/**
	 * The groupement of the association.
	 * 
	 * @var ApiFrGouvMinintRnaGroupementInterface
	 */
	protected ApiFrGouvMinintRnaGroupementInterface $_groupement;
	
	/**
	 * The titre of the association.
	 * 
	 * @var string
	 */
	protected string $_titre;
	
	/**
	 * The objet of the association.
	 * 
	 * @var string
	 */
	protected string $_objet;
	
	/**
	 * The 1st objet social of this association.
	 * 
	 * @var ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	protected ?ApiFrGouvMinintRnaObjetSocialInterface $_objetSocial1 = null;
	
	/**
	 * The 2nd objet social of this association.
	 * 
	 * @var ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	protected ?ApiFrGouvMinintRnaObjetSocialInterface $_objetSocial2 = null;
	
	/**
	 * The 1st line of address.
	 * 
	 * @var string
	 */
	protected string $_adr1;
	
	/**
	 * The 2nd line of address.
	 * 
	 * @var ?string
	 */
	protected ?string $_adr2 = null;
	
	/**
	 * The 3rd line of address.
	 * 
	 * @var ?string
	 */
	protected ?string $_adr3 = null;
	
	/**
	 * Code postal of the adresse siege.
	 * 
	 * @var string
	 */
	protected string $_adrsCodepostal;
	
	/**
	 * Libelle commune of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_libcom = null;
	
	/**
	 * Code insee of the adresse siege.
	 * 
	 * @var ?string
	 */
	protected ?string $_adrsCodeinsee = null;
	
	/**
	 * Civilite of the dirigeant.
	 * 
	 * @var string
	 */
	protected string $_dirCivilite;
	
	/**
	 * The uri of the website = null; if any.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_siteweb = null;
	
	/**
	 * The observations on this association.
	 * 
	 * @var string
	 */
	protected string $_observation;
	
	/**
	 * The position of this association.
	 * 
	 * @var ApiFrGouvMinintRnaPositionInterface
	 */
	protected ApiFrGouvMinintRnaPositionInterface $_position;
	
	/**
	 * The number "reconnaissance d'utilité publique".
	 * 
	 * @var ?string
	 */
	protected ?string $_rupMi = null;
	
	/**
	 * The date of last update.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_majTime;
	
	/**
	 * Constructor for ApiFrGouvMinintRnaAssociationImport with private members.
	 * 
	 * @param string $id
	 * @param string $idEx
	 * @param string $gestion
	 * @param DateTimeInterface $dateCreat
	 * @param DateTimeInterface $datePubli
	 * @param ApiFrGouvMinintRnaNatureInterface $nature
	 * @param ApiFrGouvMinintRnaGroupementInterface $groupement
	 * @param string $titre
	 * @param string $objet
	 * @param string $adr1
	 * @param string $adrsCodepostal
	 * @param string $dirCivilite
	 * @param string $observation
	 * @param ApiFrGouvMinintRnaPositionInterface $position
	 * @param DateTimeInterface $majTime
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(string $id, string $idEx, string $gestion, DateTimeInterface $dateCreat, DateTimeInterface $datePubli, ApiFrGouvMinintRnaNatureInterface $nature, ApiFrGouvMinintRnaGroupementInterface $groupement, string $titre, string $objet, string $adr1, string $adrsCodepostal, string $dirCivilite, string $observation, ApiFrGouvMinintRnaPositionInterface $position, DateTimeInterface $majTime)
	{
		$this->setId($id);
		$this->setIdEx($idEx);
		$this->setGestion($gestion);
		$this->setDateCreat($dateCreat);
		$this->setDatePubli($datePubli);
		$this->setNature($nature);
		$this->setGroupement($groupement);
		$this->setTitre($titre);
		$this->setObjet($objet);
		$this->setAdr1($adr1);
		$this->setAdrsCodepostal($adrsCodepostal);
		$this->setDirCivilite($dirCivilite);
		$this->setObservation($observation);
		$this->setPosition($position);
		$this->setMajTime($majTime);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the old identifier.
	 * 
	 * @param string $id
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setId(string $id) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the old identifier.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the old² identifier.
	 * 
	 * @param string $idEx
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setIdEx(string $idEx) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_idEx = $idEx;
		
		return $this;
	}
	
	/**
	 * Gets the old² identifier.
	 * 
	 * @return string
	 */
	public function getIdEx() : string
	{
		return $this->_idEx;
	}
	
	/**
	 * Sets the siret of the etablissement.
	 * 
	 * @param ?string $siret
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setSiret(?string $siret) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_siret = $siret;
		
		return $this;
	}
	
	/**
	 * Gets the siret of the etablissement.
	 * 
	 * @return ?string
	 */
	public function getSiret() : ?string
	{
		return $this->_siret;
	}
	
	/**
	 * Sets the code gestion of the prefecture.
	 * 
	 * @param string $gestion
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setGestion(string $gestion) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_gestion = $gestion;
		
		return $this;
	}
	
	/**
	 * Gets the code gestion of the prefecture.
	 * 
	 * @return string
	 */
	public function getGestion() : string
	{
		return $this->_gestion;
	}
	
	/**
	 * Sets the date when this association was created.
	 * 
	 * @param DateTimeInterface $dateCreat
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setDateCreat(DateTimeInterface $dateCreat) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_dateCreat = $dateCreat;
		
		return $this;
	}
	
	/**
	 * Gets the date when this association was created.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateCreat() : DateTimeInterface
	{
		return $this->_dateCreat;
	}
	
	/**
	 * Sets the date when this association was published.
	 * 
	 * @param DateTimeInterface $datePubli
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setDatePubli(DateTimeInterface $datePubli) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_datePubli = $datePubli;
		
		return $this;
	}
	
	/**
	 * Gets the date when this association was published.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDatePubli() : DateTimeInterface
	{
		return $this->_datePubli;
	}
	
	/**
	 * Sets the nature of the association.
	 * 
	 * @param ApiFrGouvMinintRnaNatureInterface $nature
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setNature(ApiFrGouvMinintRnaNatureInterface $nature) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_nature = $nature;
		
		return $this;
	}
	
	/**
	 * Gets the nature of the association.
	 * 
	 * @return ApiFrGouvMinintRnaNatureInterface
	 */
	public function getNature() : ApiFrGouvMinintRnaNatureInterface
	{
		return $this->_nature;
	}
	
	/**
	 * Sets the groupement of the association.
	 * 
	 * @param ApiFrGouvMinintRnaGroupementInterface $groupement
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setGroupement(ApiFrGouvMinintRnaGroupementInterface $groupement) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_groupement = $groupement;
		
		return $this;
	}
	
	/**
	 * Gets the groupement of the association.
	 * 
	 * @return ApiFrGouvMinintRnaGroupementInterface
	 */
	public function getGroupement() : ApiFrGouvMinintRnaGroupementInterface
	{
		return $this->_groupement;
	}
	
	/**
	 * Sets the titre of the association.
	 * 
	 * @param string $titre
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setTitre(string $titre) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_titre = $titre;
		
		return $this;
	}
	
	/**
	 * Gets the titre of the association.
	 * 
	 * @return string
	 */
	public function getTitre() : string
	{
		return $this->_titre;
	}
	
	/**
	 * Sets the objet of the association.
	 * 
	 * @param string $objet
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setObjet(string $objet) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_objet = $objet;
		
		return $this;
	}
	
	/**
	 * Gets the objet of the association.
	 * 
	 * @return string
	 */
	public function getObjet() : string
	{
		return $this->_objet;
	}
	
	/**
	 * Sets the 1st objet social of this association.
	 * 
	 * @param ?ApiFrGouvMinintRnaObjetSocialInterface $objetSocial1
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setObjetSocial1(?ApiFrGouvMinintRnaObjetSocialInterface $objetSocial1) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_objetSocial1 = $objetSocial1;
		
		return $this;
	}
	
	/**
	 * Gets the 1st objet social of this association.
	 * 
	 * @return ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function getObjetSocial1() : ?ApiFrGouvMinintRnaObjetSocialInterface
	{
		return $this->_objetSocial1;
	}
	
	/**
	 * Sets the 2nd objet social of this association.
	 * 
	 * @param ?ApiFrGouvMinintRnaObjetSocialInterface $objetSocial2
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setObjetSocial2(?ApiFrGouvMinintRnaObjetSocialInterface $objetSocial2) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_objetSocial2 = $objetSocial2;
		
		return $this;
	}
	
	/**
	 * Gets the 2nd objet social of this association.
	 * 
	 * @return ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function getObjetSocial2() : ?ApiFrGouvMinintRnaObjetSocialInterface
	{
		return $this->_objetSocial2;
	}
	
	/**
	 * Sets the 1st line of address.
	 * 
	 * @param string $adr1
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setAdr1(string $adr1) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_adr1 = $adr1;
		
		return $this;
	}
	
	/**
	 * Gets the 1st line of address.
	 * 
	 * @return string
	 */
	public function getAdr1() : string
	{
		return $this->_adr1;
	}
	
	/**
	 * Sets the 2nd line of address.
	 * 
	 * @param ?string $adr2
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setAdr2(?string $adr2) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_adr2 = $adr2;
		
		return $this;
	}
	
	/**
	 * Gets the 2nd line of address.
	 * 
	 * @return ?string
	 */
	public function getAdr2() : ?string
	{
		return $this->_adr2;
	}
	
	/**
	 * Sets the 3rd line of address.
	 * 
	 * @param ?string $adr3
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setAdr3(?string $adr3) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_adr3 = $adr3;
		
		return $this;
	}
	
	/**
	 * Gets the 3rd line of address.
	 * 
	 * @return ?string
	 */
	public function getAdr3() : ?string
	{
		return $this->_adr3;
	}
	
	/**
	 * Sets code postal of the adresse siege.
	 * 
	 * @param string $adrsCodepostal
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setAdrsCodepostal(string $adrsCodepostal) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_adrsCodepostal = $adrsCodepostal;
		
		return $this;
	}
	
	/**
	 * Gets code postal of the adresse siege.
	 * 
	 * @return string
	 */
	public function getAdrsCodepostal() : string
	{
		return $this->_adrsCodepostal;
	}
	
	/**
	 * Sets libelle commune of the adresse siege.
	 * 
	 * @param ?string $libcom
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setLibcom(?string $libcom) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_libcom = $libcom;
		
		return $this;
	}
	
	/**
	 * Gets libelle commune of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getLibcom() : ?string
	{
		return $this->_libcom;
	}
	
	/**
	 * Sets code insee of the adresse siege.
	 * 
	 * @param ?string $adrsCodeinsee
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setAdrsCodeinsee(?string $adrsCodeinsee) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_adrsCodeinsee = $adrsCodeinsee;
		
		return $this;
	}
	
	/**
	 * Gets code insee of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsCodeinsee() : ?string
	{
		return $this->_adrsCodeinsee;
	}
	
	/**
	 * Sets civilite of the dirigeant.
	 * 
	 * @param string $dirCivilite
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setDirCivilite(string $dirCivilite) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_dirCivilite = $dirCivilite;
		
		return $this;
	}
	
	/**
	 * Gets civilite of the dirigeant.
	 * 
	 * @return string
	 */
	public function getDirCivilite() : string
	{
		return $this->_dirCivilite;
	}
	
	/**
	 * Sets the uri of the website = null; if any.
	 * 
	 * @param ?UriInterface $siteweb
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setSiteweb(?UriInterface $siteweb) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_siteweb = $siteweb;
		
		return $this;
	}
	
	/**
	 * Gets the uri of the website = null; if any.
	 * 
	 * @return ?UriInterface
	 */
	public function getSiteweb() : ?UriInterface
	{
		return $this->_siteweb;
	}
	
	/**
	 * Sets the observations on this association.
	 * 
	 * @param string $observation
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setObservation(string $observation) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_observation = $observation;
		
		return $this;
	}
	
	/**
	 * Gets the observations on this association.
	 * 
	 * @return string
	 */
	public function getObservation() : string
	{
		return $this->_observation;
	}
	
	/**
	 * Sets the position of this association.
	 * 
	 * @param ApiFrGouvMinintRnaPositionInterface $position
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setPosition(ApiFrGouvMinintRnaPositionInterface $position) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_position = $position;
		
		return $this;
	}
	
	/**
	 * Gets the position of this association.
	 * 
	 * @return ApiFrGouvMinintRnaPositionInterface
	 */
	public function getPosition() : ApiFrGouvMinintRnaPositionInterface
	{
		return $this->_position;
	}
	
	/**
	 * Sets the number "reconnaissance d'utilité publique".
	 * 
	 * @param ?string $rupMi
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setRupMi(?string $rupMi) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_rupMi = $rupMi;
		
		return $this;
	}
	
	/**
	 * Gets the number "reconnaissance d'utilité publique".
	 * 
	 * @return ?string
	 */
	public function getRupMi() : ?string
	{
		return $this->_rupMi;
	}
	
	/**
	 * Sets the date of last update.
	 * 
	 * @param DateTimeInterface $majTime
	 * @return ApiFrGouvMinintRnaAssociationImportInterface
	 */
	public function setMajTime(DateTimeInterface $majTime) : ApiFrGouvMinintRnaAssociationImportInterface
	{
		$this->_majTime = $majTime;
		
		return $this;
	}
	
	/**
	 * Gets the date of last update.
	 * 
	 * @return DateTimeInterface
	 */
	public function getMajTime() : DateTimeInterface
	{
		return $this->_majTime;
	}
	
}
