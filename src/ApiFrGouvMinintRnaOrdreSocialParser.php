<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * MinintRnaOrdreSocialParser class file.
 * 
 * This class parses minint rna ordre social into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrGouvMinintRnaOrdreSocial>
 */
class ApiFrGouvMinintRnaOrdreSocialParser extends AbstractParser
{
	
	/**
	 * The ordres sociaux.
	 * 
	 * @var array<integer, ApiFrGouvMinintRnaOrdreSocial>
	 */
	protected array $_ordresSociaux = [];
	
	/**
	 * Builds a new MinintRnaOrdreSocialParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/ordre_social.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrGouvMinintRnaOrdreSocial::class, $csvIterator);
		
		/** @var ApiFrGouvMinintRnaOrdreSocial $ordreSocial */
		foreach($iterator as $ordreSocial)
		{
			$this->_ordresSociaux[(int) $ordreSocial->getId()] = $ordreSocial;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrGouvMinintRnaOrdreSocial
	{
		$pos = (int) \trim((string) $data);
		
		$pos = ((int) ($pos / 1000)) * 1000; // trim 3 last digits as zeroes
		
		if(isset($this->_ordresSociaux[$pos]))
		{
			return $this->_ordresSociaux[$pos];
		}
		
		throw new ParseException(ApiFrGouvMinintRnaOrdreSocial::class, $data, 0);
	}
	
}
