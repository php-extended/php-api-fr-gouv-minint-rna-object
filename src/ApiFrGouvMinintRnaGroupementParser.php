<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * MinintRnaGroupementParser class file.
 * 
 * This class parses minint rna groupements into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrGouvMinintRnaGroupement>
 */
class ApiFrGouvMinintRnaGroupementParser extends AbstractParser
{
	
	/**
	 * The groupements.
	 * 
	 * @var array<string, ApiFrGouvMinintRnaGroupement>
	 */
	protected array $_groupements = [];
	
	/**
	 * Builds a new MinintRnaGroupementParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/groupement.csv';
		$csvIterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrGouvMinintRnaGroupement::class, $csvIterator);
		
		/** @var ApiFrGouvMinintRnaGroupement $groupement */
		foreach($iterator as $groupement)
		{
			$this->_groupements[(string) $groupement->getCode()] = $groupement;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrGouvMinintRnaGroupement
	{
		$data = \trim((string) $data);
		
		if(empty($data))
		{
			$data = 'S';
		}
		
		if(isset($this->_groupements[$data]))
		{
			return $this->_groupements[$data];
		}
		
		throw new ParseException(ApiFrGouvMinintRnaGroupement::class, $data, 0);
	}
	
}
