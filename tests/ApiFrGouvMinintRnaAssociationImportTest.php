<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaAssociationImport;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaGroupement;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaNature;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaObjetSocial;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaPosition;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvMinintRnaAssociationImportTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaAssociationImport
 * @internal
 * @small
 */
class ApiFrGouvMinintRnaAssociationImportTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvMinintRnaAssociationImport
	 */
	protected ApiFrGouvMinintRnaAssociationImport $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getId());
		$expected = 'qsdfghjklm';
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetIdEx() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdEx());
		$expected = 'qsdfghjklm';
		$this->_object->setIdEx($expected);
		$this->assertEquals($expected, $this->_object->getIdEx());
	}
	
	public function testGetSiret() : void
	{
		$this->assertNull($this->_object->getSiret());
		$expected = 'qsdfghjklm';
		$this->_object->setSiret($expected);
		$this->assertEquals($expected, $this->_object->getSiret());
	}
	
	public function testGetGestion() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getGestion());
		$expected = 'qsdfghjklm';
		$this->_object->setGestion($expected);
		$this->assertEquals($expected, $this->_object->getGestion());
	}
	
	public function testGetDateCreat() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDateCreat());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateCreat($expected);
		$this->assertEquals($expected, $this->_object->getDateCreat());
	}
	
	public function testGetDatePubli() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDatePubli());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDatePubli($expected);
		$this->assertEquals($expected, $this->_object->getDatePubli());
	}
	
	public function testGetNature() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvMinintRnaNature::class)->disableOriginalConstructor()->getMock(), $this->_object->getNature());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaNature::class)->disableOriginalConstructor()->getMock();
		$this->_object->setNature($expected);
		$this->assertEquals($expected, $this->_object->getNature());
	}
	
	public function testGetGroupement() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvMinintRnaGroupement::class)->disableOriginalConstructor()->getMock(), $this->_object->getGroupement());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaGroupement::class)->disableOriginalConstructor()->getMock();
		$this->_object->setGroupement($expected);
		$this->assertEquals($expected, $this->_object->getGroupement());
	}
	
	public function testGetTitre() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getTitre());
		$expected = 'qsdfghjklm';
		$this->_object->setTitre($expected);
		$this->assertEquals($expected, $this->_object->getTitre());
	}
	
	public function testGetObjet() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getObjet());
		$expected = 'qsdfghjklm';
		$this->_object->setObjet($expected);
		$this->assertEquals($expected, $this->_object->getObjet());
	}
	
	public function testGetObjetSocial1() : void
	{
		$this->assertNull($this->_object->getObjetSocial1());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaObjetSocial::class)->disableOriginalConstructor()->getMock();
		$this->_object->setObjetSocial1($expected);
		$this->assertEquals($expected, $this->_object->getObjetSocial1());
	}
	
	public function testGetObjetSocial2() : void
	{
		$this->assertNull($this->_object->getObjetSocial2());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaObjetSocial::class)->disableOriginalConstructor()->getMock();
		$this->_object->setObjetSocial2($expected);
		$this->assertEquals($expected, $this->_object->getObjetSocial2());
	}
	
	public function testGetAdr1() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getAdr1());
		$expected = 'qsdfghjklm';
		$this->_object->setAdr1($expected);
		$this->assertEquals($expected, $this->_object->getAdr1());
	}
	
	public function testGetAdr2() : void
	{
		$this->assertNull($this->_object->getAdr2());
		$expected = 'qsdfghjklm';
		$this->_object->setAdr2($expected);
		$this->assertEquals($expected, $this->_object->getAdr2());
	}
	
	public function testGetAdr3() : void
	{
		$this->assertNull($this->_object->getAdr3());
		$expected = 'qsdfghjklm';
		$this->_object->setAdr3($expected);
		$this->assertEquals($expected, $this->_object->getAdr3());
	}
	
	public function testGetAdrsCodepostal() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getAdrsCodepostal());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsCodepostal($expected);
		$this->assertEquals($expected, $this->_object->getAdrsCodepostal());
	}
	
	public function testGetLibcom() : void
	{
		$this->assertNull($this->_object->getLibcom());
		$expected = 'qsdfghjklm';
		$this->_object->setLibcom($expected);
		$this->assertEquals($expected, $this->_object->getLibcom());
	}
	
	public function testGetAdrsCodeinsee() : void
	{
		$this->assertNull($this->_object->getAdrsCodeinsee());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsCodeinsee($expected);
		$this->assertEquals($expected, $this->_object->getAdrsCodeinsee());
	}
	
	public function testGetDirCivilite() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getDirCivilite());
		$expected = 'qsdfghjklm';
		$this->_object->setDirCivilite($expected);
		$this->assertEquals($expected, $this->_object->getDirCivilite());
	}
	
	public function testGetSiteweb() : void
	{
		$this->assertNull($this->_object->getSiteweb());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setSiteweb($expected);
		$this->assertEquals($expected, $this->_object->getSiteweb());
	}
	
	public function testGetObservation() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getObservation());
		$expected = 'qsdfghjklm';
		$this->_object->setObservation($expected);
		$this->assertEquals($expected, $this->_object->getObservation());
	}
	
	public function testGetPosition() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvMinintRnaPosition::class)->disableOriginalConstructor()->getMock(), $this->_object->getPosition());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaPosition::class)->disableOriginalConstructor()->getMock();
		$this->_object->setPosition($expected);
		$this->assertEquals($expected, $this->_object->getPosition());
	}
	
	public function testGetRupMi() : void
	{
		$this->assertNull($this->_object->getRupMi());
		$expected = 'qsdfghjklm';
		$this->_object->setRupMi($expected);
		$this->assertEquals($expected, $this->_object->getRupMi());
	}
	
	public function testGetMajTime() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getMajTime());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setMajTime($expected);
		$this->assertEquals($expected, $this->_object->getMajTime());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvMinintRnaAssociationImport('azertyuiop', 'azertyuiop', 'azertyuiop', DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->getMockBuilder(ApiFrGouvMinintRnaNature::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrGouvMinintRnaGroupement::class)->disableOriginalConstructor()->getMock(), 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', $this->getMockBuilder(ApiFrGouvMinintRnaPosition::class)->disableOriginalConstructor()->getMock(), DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'));
	}
	
}
