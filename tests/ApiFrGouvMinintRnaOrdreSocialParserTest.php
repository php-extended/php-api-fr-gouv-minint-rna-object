<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaOrdreSocialParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * MinintRnaOrdreSocialParserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaOrdreSocialParser
 *
 * @internal
 *
 * @small
 */
class ApiFrGouvMinintRnaOrdreSocialParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvMinintRnaOrdreSocialParser
	 */
	protected ApiFrGouvMinintRnaOrdreSocialParser $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItThrows() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('100000');
	}
	
	public function testItThrows2() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('0');
	}
	
	public function testItWorks() : void
	{
		$this->assertNotNull($this->_object->parse('10000'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvMinintRnaOrdreSocialParser();
	}
	
}
