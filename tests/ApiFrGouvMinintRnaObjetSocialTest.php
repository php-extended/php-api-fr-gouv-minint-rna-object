<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna\Test;

use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaObjetSocial;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaOrdreSocial;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvMinintRnaObjetSocialTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaObjetSocial
 * @internal
 * @small
 */
class ApiFrGouvMinintRnaObjetSocialTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvMinintRnaObjetSocial
	 */
	protected ApiFrGouvMinintRnaObjetSocial $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(12, $this->_object->getId());
		$expected = 25;
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetOrdreSocial() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvMinintRnaOrdreSocial::class)->disableOriginalConstructor()->getMock(), $this->_object->getOrdreSocial());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaOrdreSocial::class)->disableOriginalConstructor()->getMock();
		$this->_object->setOrdreSocial($expected);
		$this->assertEquals($expected, $this->_object->getOrdreSocial());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getName());
		$expected = 'qsdfghjklm';
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvMinintRnaObjetSocial(12, $this->getMockBuilder(ApiFrGouvMinintRnaOrdreSocial::class)->disableOriginalConstructor()->getMock(), 'azertyuiop');
	}
	
}
