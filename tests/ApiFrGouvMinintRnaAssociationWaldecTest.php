<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaAssociationWaldec;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaGroupement;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaNature;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaObjetSocial;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaPosition;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvMinintRnaAssociationWaldecTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaAssociationWaldec
 * @internal
 * @small
 */
class ApiFrGouvMinintRnaAssociationWaldecTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvMinintRnaAssociationWaldec
	 */
	protected ApiFrGouvMinintRnaAssociationWaldec $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getId());
		$expected = 'qsdfghjklm';
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetIdEx() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdEx());
		$expected = 'qsdfghjklm';
		$this->_object->setIdEx($expected);
		$this->assertEquals($expected, $this->_object->getIdEx());
	}
	
	public function testGetSiret() : void
	{
		$this->assertNull($this->_object->getSiret());
		$expected = 'qsdfghjklm';
		$this->_object->setSiret($expected);
		$this->assertEquals($expected, $this->_object->getSiret());
	}
	
	public function testGetRupMi() : void
	{
		$this->assertNull($this->_object->getRupMi());
		$expected = 'qsdfghjklm';
		$this->_object->setRupMi($expected);
		$this->assertEquals($expected, $this->_object->getRupMi());
	}
	
	public function testGetGestion() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getGestion());
		$expected = 'qsdfghjklm';
		$this->_object->setGestion($expected);
		$this->assertEquals($expected, $this->_object->getGestion());
	}
	
	public function testGetDateCreat() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDateCreat());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateCreat($expected);
		$this->assertEquals($expected, $this->_object->getDateCreat());
	}
	
	public function testGetDateDecla() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDateDecla());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDecla($expected);
		$this->assertEquals($expected, $this->_object->getDateDecla());
	}
	
	public function testGetDatePubli() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDatePubli());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDatePubli($expected);
		$this->assertEquals($expected, $this->_object->getDatePubli());
	}
	
	public function testGetDateDisso() : void
	{
		$this->assertNull($this->_object->getDateDisso());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateDisso($expected);
		$this->assertEquals($expected, $this->_object->getDateDisso());
	}
	
	public function testGetNature() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvMinintRnaNature::class)->disableOriginalConstructor()->getMock(), $this->_object->getNature());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaNature::class)->disableOriginalConstructor()->getMock();
		$this->_object->setNature($expected);
		$this->assertEquals($expected, $this->_object->getNature());
	}
	
	public function testGetGroupement() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvMinintRnaGroupement::class)->disableOriginalConstructor()->getMock(), $this->_object->getGroupement());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaGroupement::class)->disableOriginalConstructor()->getMock();
		$this->_object->setGroupement($expected);
		$this->assertEquals($expected, $this->_object->getGroupement());
	}
	
	public function testGetTitre() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getTitre());
		$expected = 'qsdfghjklm';
		$this->_object->setTitre($expected);
		$this->assertEquals($expected, $this->_object->getTitre());
	}
	
	public function testGetTitreCourt() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getTitreCourt());
		$expected = 'qsdfghjklm';
		$this->_object->setTitreCourt($expected);
		$this->assertEquals($expected, $this->_object->getTitreCourt());
	}
	
	public function testGetObjet() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getObjet());
		$expected = 'qsdfghjklm';
		$this->_object->setObjet($expected);
		$this->assertEquals($expected, $this->_object->getObjet());
	}
	
	public function testGetObjetSocial1() : void
	{
		$this->assertNull($this->_object->getObjetSocial1());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaObjetSocial::class)->disableOriginalConstructor()->getMock();
		$this->_object->setObjetSocial1($expected);
		$this->assertEquals($expected, $this->_object->getObjetSocial1());
	}
	
	public function testGetObjetSocial2() : void
	{
		$this->assertNull($this->_object->getObjetSocial2());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaObjetSocial::class)->disableOriginalConstructor()->getMock();
		$this->_object->setObjetSocial2($expected);
		$this->assertEquals($expected, $this->_object->getObjetSocial2());
	}
	
	public function testGetAdrsComplement() : void
	{
		$this->assertNull($this->_object->getAdrsComplement());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsComplement($expected);
		$this->assertEquals($expected, $this->_object->getAdrsComplement());
	}
	
	public function testGetAdrsNumVoie() : void
	{
		$this->assertNull($this->_object->getAdrsNumVoie());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsNumVoie($expected);
		$this->assertEquals($expected, $this->_object->getAdrsNumVoie());
	}
	
	public function testGetAdrsRepetition() : void
	{
		$this->assertNull($this->_object->getAdrsRepetition());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsRepetition($expected);
		$this->assertEquals($expected, $this->_object->getAdrsRepetition());
	}
	
	public function testGetAdrsTypeVoie() : void
	{
		$this->assertNull($this->_object->getAdrsTypeVoie());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsTypeVoie($expected);
		$this->assertEquals($expected, $this->_object->getAdrsTypeVoie());
	}
	
	public function testGetAdrsLibvoie() : void
	{
		$this->assertNull($this->_object->getAdrsLibvoie());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsLibvoie($expected);
		$this->assertEquals($expected, $this->_object->getAdrsLibvoie());
	}
	
	public function testGetAdrsDistrib() : void
	{
		$this->assertNull($this->_object->getAdrsDistrib());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsDistrib($expected);
		$this->assertEquals($expected, $this->_object->getAdrsDistrib());
	}
	
	public function testGetAdrsCodeinsee() : void
	{
		$this->assertNull($this->_object->getAdrsCodeinsee());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsCodeinsee($expected);
		$this->assertEquals($expected, $this->_object->getAdrsCodeinsee());
	}
	
	public function testGetAdrsCodepostal() : void
	{
		$this->assertNull($this->_object->getAdrsCodepostal());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsCodepostal($expected);
		$this->assertEquals($expected, $this->_object->getAdrsCodepostal());
	}
	
	public function testGetAdrsLibcommune() : void
	{
		$this->assertNull($this->_object->getAdrsLibcommune());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrsLibcommune($expected);
		$this->assertEquals($expected, $this->_object->getAdrsLibcommune());
	}
	
	public function testGetAdrgDeclarant() : void
	{
		$this->assertNull($this->_object->getAdrgDeclarant());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrgDeclarant($expected);
		$this->assertEquals($expected, $this->_object->getAdrgDeclarant());
	}
	
	public function testGetAdrgComplemid() : void
	{
		$this->assertNull($this->_object->getAdrgComplemid());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrgComplemid($expected);
		$this->assertEquals($expected, $this->_object->getAdrgComplemid());
	}
	
	public function testGetAdrgComplemgeo() : void
	{
		$this->assertNull($this->_object->getAdrgComplemgeo());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrgComplemgeo($expected);
		$this->assertEquals($expected, $this->_object->getAdrgComplemgeo());
	}
	
	public function testGetAdrgLibvoie() : void
	{
		$this->assertNull($this->_object->getAdrgLibvoie());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrgLibvoie($expected);
		$this->assertEquals($expected, $this->_object->getAdrgLibvoie());
	}
	
	public function testGetAdrgDistrib() : void
	{
		$this->assertNull($this->_object->getAdrgDistrib());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrgDistrib($expected);
		$this->assertEquals($expected, $this->_object->getAdrgDistrib());
	}
	
	public function testGetAdrgCodepostal() : void
	{
		$this->assertNull($this->_object->getAdrgCodepostal());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrgCodepostal($expected);
		$this->assertEquals($expected, $this->_object->getAdrgCodepostal());
	}
	
	public function testGetAdrgAchemine() : void
	{
		$this->assertNull($this->_object->getAdrgAchemine());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrgAchemine($expected);
		$this->assertEquals($expected, $this->_object->getAdrgAchemine());
	}
	
	public function testGetAdrgPays() : void
	{
		$this->assertNull($this->_object->getAdrgPays());
		$expected = 'qsdfghjklm';
		$this->_object->setAdrgPays($expected);
		$this->assertEquals($expected, $this->_object->getAdrgPays());
	}
	
	public function testGetDirCivilite() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getDirCivilite());
		$expected = 'qsdfghjklm';
		$this->_object->setDirCivilite($expected);
		$this->assertEquals($expected, $this->_object->getDirCivilite());
	}
	
	public function testGetSiteweb() : void
	{
		$this->assertNull($this->_object->getSiteweb());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setSiteweb($expected);
		$this->assertEquals($expected, $this->_object->getSiteweb());
	}
	
	public function testHasPubliweb() : void
	{
		$this->assertFalse($this->_object->hasPubliweb());
		$expected = true;
		$this->_object->setPubliweb($expected);
		$this->assertTrue($this->_object->hasPubliweb());
	}
	
	public function testGetObservation() : void
	{
		$this->assertNull($this->_object->getObservation());
		$expected = 'qsdfghjklm';
		$this->_object->setObservation($expected);
		$this->assertEquals($expected, $this->_object->getObservation());
	}
	
	public function testGetPosition() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvMinintRnaPosition::class)->disableOriginalConstructor()->getMock(), $this->_object->getPosition());
		$expected = $this->getMockBuilder(ApiFrGouvMinintRnaPosition::class)->disableOriginalConstructor()->getMock();
		$this->_object->setPosition($expected);
		$this->assertEquals($expected, $this->_object->getPosition());
	}
	
	public function testGetMajTime() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getMajTime());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setMajTime($expected);
		$this->assertEquals($expected, $this->_object->getMajTime());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvMinintRnaAssociationWaldec('azertyuiop', 'azertyuiop', 'azertyuiop', DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->getMockBuilder(ApiFrGouvMinintRnaNature::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrGouvMinintRnaGroupement::class)->disableOriginalConstructor()->getMock(), 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', false, $this->getMockBuilder(ApiFrGouvMinintRnaPosition::class)->disableOriginalConstructor()->getMock(), DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'));
	}
	
}
