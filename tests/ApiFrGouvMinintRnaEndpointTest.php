<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaAssociationImport;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaAssociationWaldec;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpoint;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaGroupement;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaNature;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaObjetSocial;
use PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaPosition;
use PhpExtended\HttpClient\ZipClient;
use PhpExtended\HttpMessage\FileStream;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * MinintRnaEndpointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvMinintRna\ApiFrGouvMinintRnaEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiFrGouvMinintRnaEndpointTest extends TestCase
{
	
	/**
	 * Gets a suitable directory to download and uncompress data.
	 *
	 * @return string
	 * @throws RuntimeException
	 */
	protected static function getTempPath() : string
	{
		$base = \is_dir('/media/anastaszor/RUNTIME/') ? '/media/anastaszor/RUNTIME/' : '/tmp/';
		$real = $base.'php-extended__php-api-fr-gouv-minint-rna-object';
		if(!\is_dir($real))
		{
			if(!\mkdir($real))
			{
				throw new RuntimeException('Failed to make temp directory at '.$real);
			}
		}
		
		return $real;
	}
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvMinintRnaEndpoint
	 */
	protected ApiFrGouvMinintRnaEndpoint $_object;
	
	public function testGetLatestUploadDate() : void
	{
		$this->assertInstanceOf(DateTimeInterface::class, $this->_object->getLatestUploadDate());
	}
	
	public function testGetLatestUploadDateSample() : void
	{
		$expected = 'https://media.interieur.gouv.fr/rna/rna_waldec_20240201.zip';
		$json = \json_decode(\file_get_contents(__DIR__.'/../data/sample.json'), true);
		$this->assertEquals($expected, $this->_object->getDistributionUrl($json['distribution'], 'rna_waldec'));
	}
	
	public function testGetLatestRnaImportIterator() : void
	{
		if(\strpos($this->getTempPath(), '/tmp/') !== false)
		{
			$this->markTestSkipped('Not Enough Disk Space on CI');
			
			return;
		}
		
		$k = 0;
		
		/** @var ApiFrGouvMinintRnaAssociationImport $assoImport */
		foreach($this->_object->getLatestRnaImportIterator() as $assoImport)
		{
			$this->assertNotEmpty($assoImport->getId());
			$k++;
			
			if($k % 1000 === 0)
			{
				break;
			}
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetLatestRnaWaldecIterator() : void
	{
		if(\strpos($this->getTempPath(), '/tmp/') !== false)
		{
			$this->markTestSkipped('Not Enough Disk Space on CI');
			
			return;
		}
		
		$k = 0;
		
		/** @var ApiFrGouvMinintRnaAssociationWaldec $assoWaldec */
		foreach($this->_object->getLatestRnaWaldecIterator() as $assoWaldec)
		{
			$this->assertNotEmpty($assoWaldec->getId());
			$k++;
			
			if($k % 1000 === 0)
			{
				break;
			}
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetNatureIterator() : void
	{
		$k = 0;
		
		/** @var ApiFrGouvMinintRnaNature $nature */
		foreach($this->_object->getNatureIterator() as $nature)
		{
			$this->assertNotEmpty($nature->getId());
			$this->assertNotEmpty($nature->getCode());
			$this->assertNotEmpty($nature->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetGroupementIterator() : void
	{
		$k = 0;
		
		/** @var ApiFrGouvMinintRnaGroupement $groupement */
		foreach($this->_object->getGroupementIterator() as $groupement)
		{
			$this->assertNotEmpty($groupement->getId());
			$this->assertNotEmpty($groupement->getCode());
			$this->assertNotEmpty($groupement->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetObjetSocialIterator() : void
	{
		$k = 0;
		
		/** @var ApiFrGouvMinintRnaObjetSocial $objetSocial */
		foreach($this->_object->getObjetSocialIterator() as $objetSocial)
		{
			$this->assertNotEmpty($objetSocial->getId());
			$this->assertNotEmpty($objetSocial->getOrdreSocial());
			$this->assertNotEmpty($objetSocial->getOrdreSocial()->getId());
			$this->assertNotEmpty($objetSocial->getOrdreSocial()->getName());
			$this->assertNotEmpty($objetSocial->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	public function testGetPositionIterator() : void
	{
		$k = 0;
		
		/** @var ApiFrGouvMinintRnaPosition $position */
		foreach($this->_object->getPositionIterator() as $position)
		{
			$this->assertNotEmpty($position->getId());
			$this->assertNotEmpty($position->getCode());
			$this->assertNotEmpty($position->getName());
			$k++;
		}
		
		$this->assertGreaterThanOrEqual(1, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$response = new Response();
				$response = $response->withHeader('X-Request-Uri', $request->getUri()->__toString());
				$response = $response->withHeader('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.3');
				
				foreach($request->getHeaders() as $name => $value)
				{
					$response = $response->withAddedHeader('X-Request-Header-'.$name, $value);
				}
				
				// echo "\n".$request->getMethod().' '.$request->getUri()->__toString()."\n";
				$headers = [];
				
				$filePath = $request->getHeaderLine('X-Php-Download-File');
				$request = $request->withoutHeader('X-Php-Download-File');
				
				foreach($request->getHeaders() as $headerKey => $headerArr)
				{
					// echo "\t".$headerKey.': '.\implode(', ', $headerArr)."\n";
					$headers[] = $headerKey.': '.\implode(', ', $headerArr);
				}
				
				$context = \stream_context_create(['http' => [
					'method' => $request->getMethod(),
					'header' => \implode("\r\n", $headers)."\r\n",
					'content' => $request->getBody()->__toString(),
				]]);
				
				$http_response_header = [];
				
				if(!empty($filePath))
				{
					\copy($request->getUri()->__toString(), $filePath, $context);
					$stream = new FileStream($filePath);
				}
				else
				{
					$data = (string) \file_get_contents($request->getUri()->__toString(), false, $context);
					$stream = new StringStream($data);
				}
				
				foreach($http_response_header as $header)
				{
					if(\mb_strpos($header, 'HTTP/') !== false)
					{
						continue;
					}
					$dotpos = \mb_strpos($header, ':');
					$name = \mb_substr($header, 0, $dotpos);
					$value = \trim(\mb_substr($header, $dotpos + 1));
					$response = $response->withAddedHeader($name, $value);
				}
				
				return $response->withBody($stream);
			}
			
		};
		
		$this->_object = new ApiFrGouvMinintRnaEndpoint($this->getTempPath(), new ZipClient($client, new StreamFactory()));
	}
	
}
