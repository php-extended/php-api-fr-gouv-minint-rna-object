# php-api-fr-gouv-minint-rna-object

A library that implements the php-extended/php-api-fr-gouv-minint-rna-interface library

![coverage](https://gitlab.com/php-extended/php-api-fr-gouv-minint-rna-object/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-gouv-minint-rna-object/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-gouv-minint-rna-object ^8`


## Basic Usage

This library gives an unique endpoint : `\PhpExtended\Minint\MinintRnaEndpoint`
from which all data can be retrieved.

- For most usages, you may use the following code :

```php

use PhpExtended\Endpoint\HttpEndpoint;

/** $client \Psr\Http\Client\ClientInterface */

$endpoint = new MinintRnaEndpoint($client);

$associations = $endpoint->getLatestRnaWaldecIterator();

foreach($associations as $association)
{
	/** @var $association \PhpExtended\Minint\MinintRnaAssociationWaldec */
}


```

Be wary that the client must follow a certain number of rules regarding the
handling of files via the `X-Php-Download-File` request and response
headers for the `ZipHttpEndpoint` to be able to unzip the downloaded
file without having to use gigabytes of memory. It must also return an 
`X-Request-Uri` header to get the full uri back.


For an example of the minimal needed http client, look at the 
`MinintRnaEndpointTest` class file and find the client that is used.


If you have that much memory however, you can bypass the `ZipHttpEndpoint`
and all of the `X-Php-Download-File` and `X-Request-Uri` shenanigans
altogether.


## License

- The code is under MIT (See [license file](LICENSE)).
